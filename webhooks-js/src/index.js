import * as React from "react";
import * as ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "./index.css";


const root = ReactDOM.createRoot(document.getElementById('root'));

const JsonElement = () => {
  console.log("I'm Here")
  const verification = "369c5056accd3a3103fa98192acb412ba17d386a";
  // const header = {"verification": verification}
  return <div>
      "369c5056accd3a3103fa98192acb412ba17d386a"
  </div>

}

const TextElement = () => {
  return(<div>Hello json!</div>)
}


const router = createBrowserRouter([
  {
    path: "/",
    element: <div>Hello world!</div>,
  },
  {
    path: "/json",
    element: <JsonElement />,
  },
  {
    path: "/text",
    element: <TextElement />,
  },
]);



root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

