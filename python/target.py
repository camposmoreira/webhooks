from flask import Flask, request, jsonify


app = Flask(__name__)


@app.route('/json', methods=['POST'])
def home_json():
    verification = request.args.get('verification', '')
    print("--home_json--")
    print(" - Verification ", verification)
    print(" - Request ", request)
    print(" - Data ", request.data)
    print("-----")
    return jsonify({"verification": verification})


@app.route('/text/', methods=['POST'])
def home_text():
    code = request.args.get("custom_headers")
    print("--code--")
    print(" code: ", code)
    print("-------")
    return code


app.run()
